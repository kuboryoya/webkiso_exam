<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            table{
                border: 1px solid #555;
            }
            thead td{
                text-align: center;
                background-color: #999;
                color: white;
            }
            #calend-sunday tbody td:first-child{
                color: red;
            }
            #calend-sunday tbody td:nth-child(7){
                color: blue;
            }
            #calend-monday tbody td:nth-child(7){
                color: red;
            }
            #calend-monday tbody td:nth-child(6){
                color: blue;
            }
            tbody td{
                text-align: right;
            }
            .today{
                background-color: yellow;
                font-weight: bold;
            }
        </style>
    </head>
    <body>

<form method="post" action="" enctypenctype="multipart/form-data">
    年 <input type="number" name="year"><br>
    月 <input type="number" name="month"><br>
    日 <input type="number" name="day"><br>
    <input type="submit" name="post_time" value="日曜始まり">
    <input type="submit" name="post_time_monday" value="月曜始まり">
</form>

<hr>

<?php

//曜日選択可能に
$sunday = false;
$monday = false;
if( !empty($_POST['post_time_monday']) ){
    $monday = true;
}else{
    $sunday = true;
}

//何も入力されてなかったら今日のやつ出す。
if( empty($_POST['year']) ){
    $_POST['year'] = date(Y);
}
if( empty($_POST['month']) ){
    $_POST['month'] = date(m);
}
if( empty($_POST['day']) ){
    $_POST['day'] = date(d);
}

$timestamp_begin = mktime(0, 0, 0, $_POST['month']    , 01           , $_POST['year']);
$timestamp       = mktime(0, 0, 0, $_POST['month']    , $_POST['day'], $_POST['year']);
$timestamp_end   = mktime(0, 0, 0, $_POST['month'] + 1, 0            , $_POST['year']);

if($sunday){
    $begin_week = date( 'w', $timestamp_begin);
}elseif($monday){
    $begin_week = date( 'N', $timestamp_begin-1);
}
$end_day = date( 't', $timestamp_end);
$today = date( 'd', $timestamp);

echo date('Y年m月d日' , $timestamp);
if($sunday){
echo '<table id="calend-sunday">';
echo <<<EOL
    <thead>
        <td>日</td>
        <td>月</td>
        <td>火</td>
        <td>水</td>
        <td>木</td>
        <td>金</td>
        <td>土</td>
    </thead>
    <tbody>
EOL;
}elseif($monday){
echo '<table id="calend-monday">';
echo <<<EOL
    <thead>
        <td>月</td>
        <td>火</td>
        <td>水</td>
        <td>木</td>
        <td>金</td>
        <td>土</td>
        <td>日</td>
    </thead>
    <tbody>
EOL;
}


$break_math = false;
$week_num = 0;
$calendar = false;
$carrent_day = 1;

echo '<tr>';
for($i=0; $i<$end_day + $begin_week; $i++){
    echo'<td>';
    if($week_num == $begin_week){
        $calendar = true;
    }
    if($calendar && $carrent_day == $today) { 
        echo '<span class="today">';
        echo $carrent_day;
        echo '</span>';
        $carrent_day ++;
    }elseif($calendar) {
        echo $carrent_day;
        $carrent_day ++;
        
    }
    echo'</td>';
    $week_num++;
    if($week_num == 7){
        $week_num = 0;
        echo'</tr>';
        echo'<tr>';
    }
}

echo <<<EOL
    </tbody>
</table>
EOL;

?>

    </body>
</html>
