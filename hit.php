<?php
session_set_cookie_params(3600);
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php

//リセット
if($_POST['post_count']){
    $_SESSION["count"] = 0;
    echo '回数をリセットしました<hr>';
}
if($_POST['post_ans_count']){
    $_SESSION["count"] = 0;
    unset($_SESSION['nums']);
    echo '回数と答えをリセットしました<hr>';
}

//回答作成
if(empty($_SESSION['nums'])){
    //0から9までシャッフル
    $array = [0,1,2,3,4,5,6,7,8,9];
    shuffle($array);
    $count = 0;
    //4つめまでの数字を使用
    foreach ($array as $number) {
        $count++;
        if($count<=4){
            $_SESSION['nums'][] = $number;
        }
    }    
}

//答えを入力されたらチェック
if(!empty($_POST['post_num'])){
    $nums = [intval($_POST['num1']),intval($_POST['num2']),intval($_POST['num3']),intval($_POST['num4'])];
    echo 'POST : '. $nums[0]. $nums[1]. $nums[2]. $nums[3] . '<hr>';

    //hit&blow
    $hitcount = 0;
    $blowcount = 0;

    for($hitblow=0; $hitblow<4; $hitblow++){
        if($nums[$hitblow] == $_SESSION['nums'][$hitblow]){
           $hitcount ++; 
        }else{
            for($m=0; $m<4; $m++){
                if($nums[$hitblow] == $_SESSION['nums'][$m]){
                 $blowcount++;   
                }
            }
        }
    }

    echo $hitcount. 'hit'. $blowcount. 'blow<br>';
    if($hitcount == 4){
        echo "おめでとうございます。<br>";
    }
    $_SESSION["count"]++;
    echo $_SESSION["count"].'回目<hr>';
    
}

?>


<form method="post" action="" enctypenctype="multipart/form-data">
    <input type="number" name="num1">
    <input type="number" name="num2">
    <input type="number" name="num3">
    <input type="number" name="num4">
    <input type="submit" name="post_num" value="チェック">
    <input type="submit" name="post_ans_count" value="答えと回数をリセット">
    <input type="submit" name="post_count" value="回数をリセット">
</form>



    </body>
</html>
